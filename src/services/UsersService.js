const bcrypt = require('bcrypt');
class UsersService {
    constructor() {
      this.users = [];
    }
  
    async addUser(user) {
        return new Promise(async (resolve, reject) => {
          this.users.push(user);
    
          try {
            resolve();
          } catch (error) {
            reject(error);
          }
        });
      }

      async findUser(email) {
        return new Promise((resolve, reject) => {
          try {
            const user = this.users.find(user => user.email === email);
            resolve(user || null);
          } catch (error) {
            reject(error);
          }
        });
      }

}

module.exports = UsersService;