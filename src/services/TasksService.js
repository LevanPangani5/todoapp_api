class TasksService {
    constructor() {
      this.todoTasks = {};
    }

    async addTask(email, task) {
      return new Promise(async (resolve, reject) => {
    try {
       if(!this.todoTasks[email]){
        this.todoTasks[email]= [];
        this.todoTasks[email].push(task);
        resolve(true);
        return;
       }
        const isValid= await this.checkTitle(email,task.title);
        if(isValid){
          this.todoTasks[email].unshift(task);
          resolve(true);
        }else{
          resolve(false);
        }
        } catch (error) {
          reject(error);
        }
      });
    }
  
    async loadTasks(email) {
      return new Promise(async (resolve, reject) => {
        try{
        if(!this.todoTasks[email]){
          this.todoTasks[email]=[];
       }
        resolve(this.todoTasks[email]);
        }catch(err){
          reject(err);
        }
      });
    }
    
    async checkTitle(email, title){
      return new Promise(async (resolve, reject) => {
        try{
        const tasks =await this.loadTasks(email);
        const hasTitle = tasks.some(task =>task.title === title);
        resolve(!hasTitle);
        }catch(err){
          reject(err);
        }
      });
    }

     async getDone(email,title){
          return new Promise(async (resolve,reject)=>{
            try{
              const tasks= this.todoTasks[email];
          if(!tasks){
            resolve(false);
            return;
          }

          const task = tasks.find(task => task.title === title);
          if(!task){
            console.log(title);
            resolve(false);
            return;
          }
          else{
            task.isDone=true;
            resolve(true);
            return;
          }
            }catch(err){
              reject(err);
            }
          })
     }

     async loadDones(email){
      return new Promise(async(resolve, reject)=>{
        try{
          const tasks= this.todoTasks[email];
          if(!tasks){
            resolve([]);
          }
          const dones = tasks.filter(task => task.isDone === true);
          resolve(dones);
        }catch(err){
          reject(err)
        }
      })
     }

     async editTask(email, title,newTitle, description){
      return new Promise(async (resolve, reject)=>{
        try{
          const tasks= this.todoTasks[email];
          if(!tasks){
            resolve(false);
            return;
          }
        
          const task = tasks.find(task => task.title === title);
          if(!task){
            resolve(false);
            return;
          }
          const isValid = await this.checkTitle(email,newTitle);
           console.log(isValid);
          if(!isValid){
            console.log("here", isValid);
            resolve(false);
            return;
          }
          else{
            task.title= newTitle;
            task.description=description;
            resolve(true);
          }
        }catch(err){
          reject(err)
        }
      })
     }

     async deleteTask(email, title){
      return new Promise(async(resolve, reject)=>{
        try{
          const tasks= this.todoTasks[email];
          if(!tasks){
            resolve(false);
          }
         
          const taskIndex = tasks.findIndex(task => task.title === title);
          if(taskIndex === -1){
            resolve(false);
          }else{
            this.todoTasks[email].splice(taskIndex, 1);
            resolve(true);
          }
          
        }catch(err){
          reject(err)
        }
      })
     }      
  }
  
  module.exports = TasksService;
  