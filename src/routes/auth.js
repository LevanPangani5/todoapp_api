const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
function isValidEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }
module.exports=(params)=>{
   const {usersService} =params;

    router.post('/register',async (req,res )=>{
        try{
        const { email, password } = req.body;
        
        if (!email || !password || password.length < 6 || !isValidEmail(email)) {
            return res.status(400).json({
              error: "InvalidRequest",
              message: "Email field contains an invalid email, or the length of a password is less than 6 characters. Also, when any of the fields are missing.",
            });
        }
          const isRegistered =await usersService.findUser(email);
        if (isRegistered !== null) {
            return res.status(400).json({
              error: "InvalidRequest",
              message: "Email is already registered.",
            });
          }

          const hashed =await bcrypt.hash(password,10);

          await usersService.addUser({email , hashed});
          return res.status(200).json({message:"A user was successfully registered."})
        }catch(err){
            console.error(err)
       }  
    })

    router.post('/login' , async (req, res , )=>{
        try{
            const { email, password } = req.body;
    
            if (!email || !password || password.length < 6 || !isValidEmail(email)) {
                return res.status(400).json({
                  error: "InvalidRequest",
                  message: "Email field contains an invalid email, or the length of a password is less than 6 characters. Also, when any of the fields are missing.",
                });
            }

            const user =await usersService.findUser(email);
            if (user === null) {
                return res.status(400).json({
                  error: "InvalidRequest",
                  message: "Email is not registered.",
                });
              }

              const isValid = await  bcrypt.compare(password , user.hashed);
              if(!isValid){
                return res.status(400).json({
                    error: "InvalidRequest",
                    message: "passwords does not match .",
                  });
              }
              else{
                const token = jwt.sign({ email }, 'topSecret', { expiresIn: '1h' });
                return res.status(200).json({
                    user: { 
                        email 
                  }, 
                  token
                })
              }
        }catch(err){
            console.error(err)
        }
    })
    return router;
}