const express = require('express');
const doneRoutes = require('./done')
const router = express.Router();
const jwt = require('jsonwebtoken');


async function isAuthenticated(req, res, next) {
  const authHeader = req.headers['authorization'];
  if (authHeader && authHeader.startsWith('Bearer ')) {
    const token = authHeader.split(' ')[1];  
    try {
      const decoded = jwt.verify(token, 'topSecret');
      req.user = decoded;
      next();
    } catch (err) {
      return res.status(401).json({
        error: 'Unauthorized',
        message: 'Invalid or expired token.',
      });
    }
  } else {
    return res.status(401).json({
      error: 'Unauthorized',
      message: 'Authorization token missing or invalid.',
    });
  }
}

module.exports=(params)=>{
    const {tasksService} =params;

    router.post('/',isAuthenticated ,async (req,res)=>{
      const {title,description}= req.body;
      if(!title || !description){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }
      
       const result = await tasksService.addTask(req.user.email , {title , description , isDone:false})

       if(!result){
        return res.status(400).json({
          error:"Bad Request",
          message:"task with given title aleady exists."
        })
       }else{
        return res.status(201).json({
          message:"task was added successfully "
        })
       }
    })

    router.get('/',isAuthenticated , async(req,res)=>{
      const tasks = await tasksService.loadTasks(req.user.email);
       return res.status(200).json({
          tasks
       })
    })

    router.post('/done', isAuthenticated , async(req,res)=>{
      const{title}= req.body;
      if(!title){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }

      const result = await tasksService.getDone(req.user.email , title);
      if(!result){
        return res.status(404).json({
          error:"Not Found d",
          message:"A task with the provided title does not exist in the user's to-do list"
        })
      }else{
        return res.status(200).json({
          message:"A user task was successfully marked as done ."
        })
      }

    })

    router.get('/done', isAuthenticated , async(req ,res)=>{
       const dones  = await tasksService.loadDones(req.user.email);
       return res.status(200).json({
        dones
       })
    })

    router.put('/done/:taskTitle', isAuthenticated, async (req, res)=>{
      const currentTitle = req.params.taskTitle;
      const {title,description}= req.body;
      
      if(!title || !description||!currentTitle){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }
      const result = await tasksService.editTask(req.user.email, currentTitle, title, description);
   
      if(!result ){
        return res.status(400).json({
          mesage:"A task with such taskTitle is not found in the user's tasks. or newTitle is not unique in the todolist ."
        })
      }else{
        return res.status(200).json({
          message:" task updated succesfully"
        })
      }
    })

    router.delete('/done', isAuthenticated, async(req, res)=>{
      const{title}= req.body;
      if(!title){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }

       await tasksService.deleteTask(req.user.email, title );
      
        return res.status(204).json({
          message:"A task was successfully deleted. Also, if a task with the provided title does not exist."
        })
      
    })

    return router;
}