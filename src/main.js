const express = require("express");
const makeStoppable = require("stoppable")
const http = require("http");
const path = require('path');

const cookeiSession= require('cookie-session');

const bodyParser = require('body-parser');

const TasksService = require('./services/TasksService');
const UsersService = require('./services/UsersService');
const tasksService = new TasksService();
const usersService = new UsersService();

const routes = require('./routes')
const authRoutes = require('./routes/auth')
const app = express();

app.use(
  cookeiSession({
    name:'session',
    keys:['sdfax123']
  })
)

app.use(bodyParser.urlencoded({exteded:true}));
app.use(bodyParser.json());





app.use(
  '/api/auth/',
  authRoutes({
  usersService,
}))

app.use(
  '/api/tasks/',
  routes({
  tasksService,
}))

/*app.use((err, req, res) => {
  if(err){
  console.error(err);
 return res.status(500).json({
    error: "InternalServerError",
    message: "Something went wrong on the server.",
  });
}
});*/

const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    })
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
}
